/*! \page object_locking Leviathan Locking patterns
    \tableofcontents

    \section thread_safe What is thread safe?
    - All classes that inherit from ThreadSafe or ThreadSafeRecursive
    \see Leviathan::ThreadSafeGeneric
    - Some static methods that have their own internal mutexes

    \section locking_invoking Invoking 

    If you want to call a function or a method that is not
    thread safe you must invoke the method. Invoking works by calling
    Leviathan::Engine::Invoke with a lambda to be ran once the main
    loop checks for ticking again.

    The recommended way to use this is to run expensive calculations
    in small pieces or in a QueuedTask and then invoke with the result
    and apply it in the invoke callback which can access all
    resources.

    \section locking_deadlocking Deadlocking

    Once an object is locked the lock needs to be passed to all other method calls that
    have an optional lock variant.
    Other objects may also require that you pass your lock plus the lock for the other object
    when calling methods.


    \section locking_style Style
    All methods that work on already locked objects take the lock as the first parameter.
    Methods that want locks of other objects should receive the locks as the last argument.
    If additionally a pointer to the object that is locked is passed the lock parameter should
    follow the parameter that is locked by it.

*/