#pragma once

//! \file Forward declarations for entity things
#include <inttypes.h>

// ------------------------------------ //


namespace Leviathan{

    using ObjectID = int32_t;

}
